import ie.uws.Bar
import ie.uws.Foo

class BootStrap {

    def init = { servletContext ->
        new Foo().save()
        new Bar().save()
        new Bar().save()
        new Bar().save()
        new Foo().save()
        new Bar().save()
    }
    def destroy = {
    }
}
