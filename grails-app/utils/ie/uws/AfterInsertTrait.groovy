package ie.uws

import groovy.util.logging.Log4j

/**
 * @author Marcin Szlagor
 */
@Log4j
trait AfterInsertTrait {

    def afterInsert() {
        log.info "After insert called id = ${this.id}"
    }

}
